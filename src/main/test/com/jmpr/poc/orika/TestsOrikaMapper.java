package com.jmpr.poc.orika;

import com.jmpr.beans.orika.*;
import com.jmpr.pocs.orika.mapper.OrikaCustomMapper;
import ma.glasnost.orika.MapperFacade;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class TestsOrikaMapper {
    private MapperFacade mapper;

    @Before
    public void setUp() {
        mapper = new OrikaCustomMapper().getMapper();
    };

    @Test
    public void testDefaultMapping() throws Exception {
        BeanAIn beanAIn  = new BeanAIn("value1", "value2", "value3");
        BeanAOut beanAOut = mapper.map(beanAIn, BeanAOut.class);

        Assert.assertEquals(beanAIn.getField1(), beanAOut.getField1());
        Assert.assertEquals(beanAIn.getField2(), beanAOut.getField2());
        Assert.assertEquals(beanAIn.getField3(), beanAOut.getField3());
    }

    @Test
    public void testBidirectionalMapping() throws Exception {
        BeanAOut beanAOut = new BeanAOut("value1", "value2", "value3");
        BeanAIn beanAIn = mapper.map(beanAOut, BeanAIn.class);

        Assert.assertEquals(beanAOut.getField1(), beanAIn.getField1());
        Assert.assertEquals(beanAOut.getField2(), beanAIn.getField2());
        Assert.assertEquals(beanAOut.getField3(), beanAIn.getField3());
    }

    @Test
    public void testMixedMappingUnidirectionalAndExcluding() throws Exception {
        BeanBIn beanBIn  = new BeanBIn("value1", "value2", "value3");
        BeanBOut beanBOut = mapper.map(beanBIn, BeanBOut.class);

        Assert.assertEquals(beanBIn.getFieldA(), beanBOut.getFieldAOut());
        Assert.assertEquals(beanBIn.getFieldB(), beanBOut.getFieldBOut());
        Assert.assertNull(beanBOut.getFieldC());

        BeanBIn beanBIn2 = mapper.map(beanBOut, BeanBIn.class);

        Assert.assertEquals(beanBOut.getFieldAOut(), beanBIn2.getFieldA());
        Assert.assertNull(beanBIn2.getFieldB());
        Assert.assertNull(beanBIn2.getFieldC());
    }

    @Test
    public void testConstructorMapping() throws Exception {
        BeanGIn beanGIn  = new BeanGIn("value1", "value2", "value3");
        BeanGOut beanGOut = mapper.map(beanGIn, BeanGOut.class);

        Assert.assertNotEquals("fixed", beanGOut.getField3());  // Orika always call the setters so value set in constructor is overriden
        Assert.assertEquals("fixed", beanGOut.getCustomField());
    }

    @Test
    public void testComposedMapping() throws Exception {
        BeanCIn beanCIn  = new BeanCIn("value1", "value2", "value3");
        BeanCOut beanCOut = mapper.map(beanCIn, BeanCOut.class);

        Assert.assertEquals(beanCIn.getFieldA() + beanCIn.getFieldB(), beanCOut.getFieldComposition());
    }

    @Test
    public void testConverterMapping() throws Exception {
        BeanDIn beanDIn = new BeanDIn("18/10/2013");
        BeanDOut beanDOut = mapper.map(beanDIn, BeanDOut.class);

        Assert.assertNotNull(beanDOut.getFieldA());
    }

    @Test
    public void testCustomDefaultBehaviourMapping() throws Exception {
        BeanEIn beanEIn = new BeanEIn("value");
        BeanEOut beanEOut = mapper.map(beanEIn, BeanEOut.class);

        Assert.assertEquals(beanEIn.getMyField(), beanEOut.getField());
    }

    @Test
    public void testNestedBeansMapping() throws Exception {
        BeanFIn beanFIn = new BeanFIn(new BeanEIn("value"));
        BeanFOut beanFOut = mapper.map(beanFIn, BeanFOut.class);

        Assert.assertEquals(beanFIn.getField().getMyField(), beanFOut.getProcessedField());
    }
}
