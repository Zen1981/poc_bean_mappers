package com.jmpr.poc.mapstruct;

import com.jmpr.beans.mapstruct.*;
import com.jmpr.pocs.mapstruct.enums.EnumIn;
import com.jmpr.pocs.mapstruct.enums.EnumOut;
import com.jmpr.pocs.mapstruct.mapper.ACMapper;
import com.jmpr.pocs.mapstruct.mapper.IMapper;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class TestsMapStructMapper {
    @Test
    public void testBasicMapping() throws Exception {
        // Test data
        BeanAIn bi = new BeanAIn();
        bi.setField1("value1");
        bi.setField2("value2");
        bi.setField3("value3");

        // Class Under Test execution
        BeanAOut bo = IMapper.INSTANCE.mapBeanAInToBeanAOut(bi);

        // Verifications
        Assert.assertEquals(bi.getField1(), bo.getField1Out());
        Assert.assertEquals(bi.getField2(), bo.getField2Out());
        Assert.assertEquals(bi.getField3(), bo.getField3());
    }

    @Test
    public void testCustomMapping() throws Exception {
        // Test data
        BeanBIn bi = new BeanBIn();
        bi.setField1("value1");
        bi.setField2("value2");
        bi.setField3("value3");

        // Class Under Test execution
        BeanBOut bo = ACMapper.INSTANCE.beanBInToBeanBOut(bi);

        // Verifications
        Assert.assertEquals(bi.getField1() + ".modified", bo.getField1Out());
        Assert.assertEquals(bi.getField2() + ".modified", bo.getField2Out());
        Assert.assertEquals(bi.getField1() + bi.getField2(), bo.getField3());
    }

    @Test
    public void testMergeMapping() throws Exception {
        // Test data
        BeanAIn bia = new BeanAIn();
        bia.setField1("valueA1");
        bia.setField2("valueA2");
        bia.setField3("valueA3");

        BeanBIn bib = new BeanBIn();
        bib.setField1("valueB1");
        bib.setField2("valueB2");
        bib.setField3("valueB3");

        // Class Under Test execution
        BeanABOut bo = IMapper.INSTANCE.mapBeanAinAndBeanBinToBeanABOut(bia, bib);

        // Verifications
        Assert.assertEquals(bia.getField1(), bo.getFieldA1Out());
        Assert.assertEquals(bia.getField2(), bo.getFieldA2Out());
        Assert.assertEquals(bia.getField3(), bo.getFieldA3());
        Assert.assertEquals(bib.getField1(), bo.getFieldB1Out());
        Assert.assertEquals(bib.getField2(), bo.getFieldB2Out());
        Assert.assertEquals(bib.getField3(), bo.getFieldB3());
    }

    @Test
    public void testNestedMapping() throws Exception {
        // Test data
        BeanAIn bia = new BeanAIn();
        bia.setField1("valueA1");
        bia.setField2("valueA2");
        bia.setField3("valueA3");

        BeanBIn bib = new BeanBIn();
        bib.setField1("valueB1");
        bib.setField2("valueB2");
        bib.setField3("valueB3");

        BeanCIn bic = new BeanCIn();
        bic.setField1(bia);
        bic.setField2(bib);

        // Class Under Test execution
        BeanCOut bo = IMapper.INSTANCE.mapBeanCInToBeanCOut(bic);

        // Verifications
        Assert.assertEquals(bia.getField3(), bo.getFieldCOut1());
        Assert.assertEquals(bib.getField3(), bo.getFieldCOut2());
    }

    @Test
    public void testPartialMapping() throws Exception {
        // Test data
        BeanAIn bia = new BeanAIn();
        bia.setField1("valueA1");
        bia.setField2("valueA2");
        bia.setField3("valueA3");

        BeanBIn bib = new BeanBIn();
        bib.setField1("valueB1");
        bib.setField2("valueB2");
        bib.setField3("valueB3");

        BeanCOut bo = new BeanCOut();

        // Class Under Test execution (I)
        IMapper.INSTANCE.mapBeanAinToBeanCOut(bia, bo);

        // Verifications (I)
        Assert.assertNull(bo.getFieldCOut2());
        Assert.assertEquals(bia.getField3(), bo.getFieldCOut1());

        // Class Under Test execution (I)
        IMapper.INSTANCE.mapBeanBinToBeanCOut(bib, bo);

        // Verifications (I)
        Assert.assertEquals(bia.getField3(), bo.getFieldCOut1());
        Assert.assertEquals(bib.getField3(), bo.getFieldCOut2());
    }

    @Test
    public void testConversionsMapping() throws Exception {
        // Test data
        BeanDIn bid = new BeanDIn();
        bid.setDate("18/07/1981");
        bid.setFloating("4.5");
        bid.setInteger(new Integer(2));

        // Class Under Test Exection
        BeanDOut bo = IMapper.INSTANCE.mapBeanDinToBeanDOut(bid);

        // Verifications
        Assert.assertNotNull(bo.getField1());
        Assert.assertEquals(2, bo.getField2());
        Assert.assertEquals(4.5, bo.getField3(), 0.01);
    }

    @Test
    public void testChainingMapping() throws Exception {
        // Test Data
        BeanAIn bia = new BeanAIn();
        bia.setField1("valueA1");
        bia.setField2("valueA2");
        bia.setField3("valueA3");

        BeanBIn bib = new BeanBIn();
        bib.setField1("valueB1");
        bib.setField2("valueB2");
        bib.setField3("valueB3");

        BeanEIn bie = new BeanEIn();
        bie.setField1(bia);
        bie.setField2(bib);

        // Class Under Test Execution
        BeanEOut bo = IMapper.INSTANCE.mapBeanE(bie);

        // Verifications
        Assert.assertEquals(bie.getField1().getField1(), bo.getField1().getField1Out());
        Assert.assertEquals(bie.getField1().getField2(), bo.getField1().getField2Out());
        Assert.assertEquals(bie.getField1().getField3(), bo.getField1().getField3());
        Assert.assertEquals(bie.getField2().getField1(), bo.getField2().getField1Out());
        Assert.assertEquals(bie.getField2().getField2(), bo.getField2().getField2Out());
        Assert.assertEquals(bie.getField2().getField3(), bo.getField2().getField3());
    }

    @Test
    public void testCollectionsMapping() throws Exception {
        // Test Data
        BeanAIn bia1 = new BeanAIn();
        bia1.setField1("valueA1.1");
        bia1.setField2("valueA1.2");
        bia1.setField3("valueA1.3");

        BeanAIn bia2 = new BeanAIn();
        bia2.setField1("valueA2.1");
        bia2.setField2("valueA2.2");
        bia2.setField3("valueA2.3");


        BeanFIn in = new BeanFIn();
        in.setField1(Arrays.asList(bia1, bia2));

        // Class Under Test Execution
        BeanFOut bo = IMapper.INSTANCE.mapBeanF(in);

        // Verifications
        Assert.assertTrue(bo.getField1().size() == 2);
        Assert.assertEquals(bia1.getField1(), bo.getField1().get(0).getField1Out());
        Assert.assertEquals(bia1.getField2(), bo.getField1().get(0).getField2Out());
        Assert.assertEquals(bia1.getField3(), bo.getField1().get(0).getField3());

        Assert.assertEquals(bia2.getField1(), bo.getField1().get(1).getField1Out());
        Assert.assertEquals(bia2.getField2(), bo.getField1().get(1).getField2Out());
        Assert.assertEquals(bia2.getField3(), bo.getField1().get(1).getField3());
    }

    @Test
    public void testEnumMappings() throws Exception {

        // Test Data
        BeanGIn beanGIn = new BeanGIn();
        beanGIn.setField1(EnumIn.VALUE_IN_1);
        beanGIn.setField2(EnumIn.VALUE_IN_2);
        beanGIn.setField3(EnumIn.VALUE_IN_3);

        // Class Under Test Execution
        BeanGOut out = IMapper.INSTANCE.mapBeanG(beanGIn);

        // Verifications
        Assert.assertEquals(EnumOut.VALUE_OUT_3, out.getField1());
        Assert.assertEquals(EnumOut.VALUE_OUT_2, out.getField2());
        Assert.assertEquals(EnumOut.VALUE_OUT_1, out.getField3());
    }

    @Test
    public void testConstants() throws Exception {
        // Test Data
        BeanBIn bib = new BeanBIn();
        bib.setField1("valueB1");
        bib.setField2("valueB2");
        bib.setField3("valueB3");

        // Class Under Test Execution
        BeanAOut out = IMapper.INSTANCE.mapConstants(bib);

        // Verifications
        Assert.assertEquals("constant1", out.getField1Out());
        Assert.assertEquals("constant2", out.getField2Out());
        Assert.assertEquals("constant3", out.getField3());
    }
}
