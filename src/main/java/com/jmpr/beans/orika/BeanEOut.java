package com.jmpr.beans.orika;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanEOut {
    private String field;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}