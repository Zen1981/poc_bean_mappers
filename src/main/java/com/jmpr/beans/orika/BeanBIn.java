package com.jmpr.beans.orika;

/**
 * @author @author Juan Manuel Pérez Rodríguez
 */
public class BeanBIn {
    private String fieldA;
    private String fieldB;
    private String fieldC;

    public BeanBIn() {
    }

    public BeanBIn(String fieldA, String fieldB, String fieldC) {
        this.fieldA = fieldA;
        this.fieldB = fieldB;
        this.fieldC = fieldC;
    }

    public String getFieldA() {
        return fieldA;
    }

    public void setFieldA(String fieldA) {
        this.fieldA = fieldA;
    }

    public String getFieldB() {
        return fieldB;
    }

    public void setFieldB(String fieldB) {
        this.fieldB = fieldB;
    }

    public String getFieldC() {
        return fieldC;
    }

    public void setFieldC(String fieldC) {
        this.fieldC = fieldC;
    }
}
