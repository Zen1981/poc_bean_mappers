package com.jmpr.beans.orika;

import java.util.Date;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanDOut {
    private Date fieldA;
    private Date fieldB;

    public Date getFieldA() {
        return fieldA;
    }

    public void setFieldA(Date fieldA) {
        this.fieldA = fieldA;
    }

    public Date getFieldB() {
        return fieldB;
    }

    public void setFieldB(Date fieldB) {
        this.fieldB = fieldB;
    }
}