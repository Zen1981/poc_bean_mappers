package com.jmpr.beans.orika;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanCOut {
    private String fieldA;
    private String fieldComposition;    // This field will concat the other 2 fields

    public String getFieldComposition() {
        return fieldComposition;
    }

    public void setFieldComposition(String fieldComposition) {
        this.fieldComposition = fieldComposition;
    }

    public String getFieldA() {
        return fieldA;
    }

    public void setFieldA(String fieldA) {
        this.fieldA = fieldA;
    }
}
