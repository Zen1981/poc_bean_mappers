package com.jmpr.beans.orika;

/**
 * @author @author Juan Manuel Pérez Rodríguez
 */
public class BeanBOut {
    private String fieldAOut;
    private String fieldBOut;
    private String fieldC;

    public BeanBOut() {
    }

    public BeanBOut(String fieldAOut, String fieldBOut, String fieldC) {
        this.fieldAOut = fieldAOut;
        this.fieldBOut = fieldBOut;
        this.fieldC = fieldC;
    }

    public String getFieldAOut() {
        return fieldAOut;
    }

    public void setFieldAOut(String fieldAOut) {
        this.fieldAOut = fieldAOut;
    }

    public String getFieldBOut() {
        return fieldBOut;
    }

    public void setFieldBOut(String fieldBOut) {
        this.fieldBOut = fieldBOut;
    }

    public String getFieldC() {
        return fieldC;
    }

    public void setFieldC(String fieldC) {
        this.fieldC = fieldC;
    }
}
