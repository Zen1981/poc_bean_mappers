package com.jmpr.beans.orika;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanFIn {
    private BeanEIn field;

    public BeanFIn() {
    }

    public BeanFIn(BeanEIn field) {
        this.field = field;
    }

    public BeanEIn getField() {
        return field;
    }

    public void setField(BeanEIn field) {
        this.field = field;
    }
}