package com.jmpr.beans.orika;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanEIn {
    private String myField;

    public BeanEIn() {
    }

    public BeanEIn(String value) {
        this.myField = value;
    }

    public String getMyField() {
        return myField;
    }

    public void setMyField(String myField) {
        this.myField = myField;
    }
}