package com.jmpr.beans.orika;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanFOut {
    private String processedField;

    public String getProcessedField() {
        return processedField;
    }

    public void setProcessedField(String processedField) {
        this.processedField = processedField;
    }
}