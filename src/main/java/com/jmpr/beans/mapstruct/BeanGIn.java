package com.jmpr.beans.mapstruct;

import com.jmpr.pocs.mapstruct.enums.EnumIn;

import java.util.List;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanGIn {
    private EnumIn field1;
    private EnumIn field2;
    private EnumIn field3;

    public EnumIn getField1() {
        return field1;
    }

    public void setField1(EnumIn field1) {
        this.field1 = field1;
    }

    public EnumIn getField2() {
        return field2;
    }

    public void setField2(EnumIn field2) {
        this.field2 = field2;
    }

    public EnumIn getField3() {
        return field3;
    }

    public void setField3(EnumIn field3) {
        this.field3 = field3;
    }
}
