package com.jmpr.beans.mapstruct;

import java.util.List;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanFOut {
    private List<BeanAOut> field1;

    public List<BeanAOut> getField1() {
        return field1;
    }

    public void setField1(List<BeanAOut> field1) {
        this.field1 = field1;
    }
}
