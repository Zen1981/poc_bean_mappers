package com.jmpr.beans.mapstruct;

import java.util.List;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanFIn {
    private List<BeanAIn> field1;

    public List<BeanAIn> getField1() {
        return field1;
    }

    public void setField1(List<BeanAIn> field1) {
        this.field1 = field1;
    }
}
