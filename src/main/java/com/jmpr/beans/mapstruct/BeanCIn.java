package com.jmpr.beans.mapstruct;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanCIn {
    private BeanAIn field1;
    private BeanBIn field2;

    public BeanAIn getField1() {
        return field1;
    }

    public void setField1(BeanAIn field1) {
        this.field1 = field1;
    }

    public BeanBIn getField2() {
        return field2;
    }

    public void setField2(BeanBIn field2) {
        this.field2 = field2;
    }
}
