package com.jmpr.beans.mapstruct;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanABOut {
    private String fieldA1Out;
    private String fieldA2Out;
    private String fieldA3;
    private String fieldB1Out;
    private String fieldB2Out;
    private String fieldB3;

    public String getFieldA1Out() {
        return fieldA1Out;
    }

    public void setFieldA1Out(String fieldA1Out) {
        this.fieldA1Out = fieldA1Out;
    }

    public String getFieldA2Out() {
        return fieldA2Out;
    }

    public void setFieldA2Out(String fieldA2Out) {
        this.fieldA2Out = fieldA2Out;
    }

    public String getFieldA3() {
        return fieldA3;
    }

    public void setFieldA3(String fieldA3) {
        this.fieldA3 = fieldA3;
    }

    public String getFieldB1Out() {
        return fieldB1Out;
    }

    public void setFieldB1Out(String fieldB1Out) {
        this.fieldB1Out = fieldB1Out;
    }

    public String getFieldB2Out() {
        return fieldB2Out;
    }

    public void setFieldB2Out(String fieldB2Out) {
        this.fieldB2Out = fieldB2Out;
    }

    public String getFieldB3() {
        return fieldB3;
    }

    public void setFieldB3(String fieldB3) {
        this.fieldB3 = fieldB3;
    }
}
