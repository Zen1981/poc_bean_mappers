package com.jmpr.beans.mapstruct;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanAOut {
    private String field1Out;
    private String field2Out;
    private String field3;

    public String getField1Out() {
        return field1Out;
    }

    public void setField1Out(String field1Out) {
        this.field1Out = field1Out;
    }

    public String getField2Out() {
        return field2Out;
    }

    public void setField2Out(String field2Out) {
        this.field2Out = field2Out;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }
}
