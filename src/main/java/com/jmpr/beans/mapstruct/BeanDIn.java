package com.jmpr.beans.mapstruct;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanDIn {
    private String date;
    private Integer integer;
    private String floating;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public String getFloating() {
        return floating;
    }

    public void setFloating(String floating) {
        this.floating = floating;
    }
}
