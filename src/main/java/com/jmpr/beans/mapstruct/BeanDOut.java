package com.jmpr.beans.mapstruct;

import java.util.Date;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanDOut {
    private Date field1;
    private int field2;
    private float field3;

    public Date getField1() {
        return field1;
    }

    public void setField1(Date field1) {
        this.field1 = field1;
    }

    public int getField2() {
        return field2;
    }

    public void setField2(int field2) {
        this.field2 = field2;
    }

    public float getField3() {
        return field3;
    }

    public void setField3(float field3) {
        this.field3 = field3;
    }
}
