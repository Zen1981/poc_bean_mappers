package com.jmpr.beans.mapstruct;

import com.jmpr.pocs.mapstruct.enums.EnumIn;
import com.jmpr.pocs.mapstruct.enums.EnumOut;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanGOut {
    private EnumOut field1;
    private EnumOut field2;
    private EnumOut field3;

    public EnumOut getField1() {
        return field1;
    }

    public void setField1(EnumOut field1) {
        this.field1 = field1;
    }

    public EnumOut getField2() {
        return field2;
    }

    public void setField2(EnumOut field2) {
        this.field2 = field2;
    }

    public EnumOut getField3() {
        return field3;
    }

    public void setField3(EnumOut field3) {
        this.field3 = field3;
    }
}
