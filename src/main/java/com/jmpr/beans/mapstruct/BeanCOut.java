package com.jmpr.beans.mapstruct;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanCOut {
    private String fieldCOut1;
    private String fieldCOut2;

    public String getFieldCOut1() {
        return fieldCOut1;
    }

    public void setFieldCOut1(String fieldCOut1) {
        this.fieldCOut1 = fieldCOut1;
    }

    public String getFieldCOut2() {
        return fieldCOut2;
    }

    public void setFieldCOut2(String fieldCOut2) {
        this.fieldCOut2 = fieldCOut2;
    }
}
