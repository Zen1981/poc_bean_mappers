package com.jmpr.beans.mapstruct;

import java.util.Date;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class BeanEOut {
    private BeanAOut field1;
    private BeanBOut field2;

    public BeanAOut getField1() {
        return field1;
    }

    public void setField1(BeanAOut field1) {
        this.field1 = field1;
    }

    public BeanBOut getField2() {
        return field2;
    }

    public void setField2(BeanBOut field2) {
        this.field2 = field2;
    }
}
