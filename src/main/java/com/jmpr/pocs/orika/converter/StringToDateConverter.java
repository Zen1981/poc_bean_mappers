package com.jmpr.pocs.orika.converter;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.metadata.Type;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class StringToDateConverter extends CustomConverter<String, Date> {
    private SimpleDateFormat sdf;

    public StringToDateConverter(String pattern) {
        this.sdf = new SimpleDateFormat(pattern);

    }

    @Override
    public Date convert(String s, Type<? extends Date> type) {
        try {
            return sdf.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
