package com.jmpr.pocs.orika.mapper;

import com.jmpr.beans.orika.*;
import com.jmpr.pocs.orika.converter.StringToDateConverter;
import ma.glasnost.orika.DefaultFieldMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;
import ma.glasnost.orika.metadata.Type;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public class OrikaCustomMapper {
    private MapperFactory mapperFactoryNoAutoMapping = new DefaultMapperFactory.Builder().useAutoMapping(false).build();
    private MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

    public OrikaCustomMapper() {
        // Automapping (default)
        mapperFactory.classMap(BeanAIn.class, BeanAOut.class)
                .byDefault()
                .register();

        // Custom mapping + automapping
        // Note: field() define mapping bidirectionally
        mapperFactory.classMap(BeanBIn.class, BeanBOut.class)
                .field("fieldA", "fieldAOut")
                .fieldAToB("fieldB", "fieldBOut")
                .register();
        ;

        // Mapping defining constructors to use
        mapperFactory.classMap(BeanGIn.class, BeanGOut.class)
            .constructorB("field1", "field2")
            .byDefault()
            .register();
        ;

        // Custom mapping (composition)
        ClassMapBuilder<BeanCIn, BeanCOut> builder = mapperFactory.classMap(BeanCIn.class, BeanCOut.class);
        builder.customize(new ma.glasnost.orika.CustomMapper<BeanCIn, BeanCOut>() {

            @Override
            public void mapAtoB(BeanCIn beanCIn, BeanCOut beanCOut, MappingContext context) {
                beanCOut.setFieldComposition(beanCIn.getFieldA() + beanCIn.getFieldB());
            }
        });
        mapperFactory.registerClassMap(builder.byDefault().toClassMap());

        // Mapping with converters
        ConverterFactory converterFactory = mapperFactory.getConverterFactory();
        converterFactory.registerConverter("dateConverter1", new StringToDateConverter("dd/MM/yyyy"));
        converterFactory.registerConverter("dateConverter2", new StringToDateConverter("dd-MM-yyyy"));

        mapperFactory.registerClassMap(mapperFactory.classMap(BeanDIn.class, BeanDOut.class).fieldMap("fieldA").converter("dateConverter1").add().toClassMap());

        // Customization of default mapping
        DefaultFieldMapper myDefaultMapper = new DefaultFieldMapper() {
            @Override
            public String suggestMappedField(String s, Type<?> type) {
                if(s.startsWith("my")) {
                    // Remove the "my" prefix and adjust camel-case
                    return s.substring(2,3).toLowerCase() +
                            s.substring(3);
                } else {
                    return s;
                }
            }
        };
        mapperFactory.registerDefaultFieldMapper(myDefaultMapper);
        mapperFactory.registerClassMap(
                mapperFactory.classMap(BeanEIn.class, BeanEOut.class)
                        .byDefault(myDefaultMapper)
                        .toClassMap()
        );

        // Nested beans mapping
        mapperFactory.registerClassMap(
                mapperFactory.classMap(BeanFIn.class,BeanFOut.class)
                        .field("field.myField", "processedField")
                        .byDefault()
                        .toClassMap()
        );
    }

    public MapperFacade getMapper() {
        return mapperFactory.getMapperFacade();
    }

    public MapperFacade getMapperNoAutomapping() {
        return mapperFactoryNoAutoMapping.getMapperFacade();
    }
}
