package com.jmpr.pocs.mapstruct.enums;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public enum EnumIn {
    VALUE_IN_1, VALUE_IN_2, VALUE_IN_3
}
