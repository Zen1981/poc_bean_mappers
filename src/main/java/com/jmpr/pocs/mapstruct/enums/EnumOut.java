package com.jmpr.pocs.mapstruct.enums;

/**
 * @author Juan Manuel Pérez Rodríguez
 */
public enum EnumOut {
    VALUE_OUT_1, VALUE_OUT_2, VALUE_OUT_3
}
