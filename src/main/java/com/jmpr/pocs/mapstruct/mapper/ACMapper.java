package com.jmpr.pocs.mapstruct.mapper;

import com.jmpr.beans.mapstruct.BeanAIn;
import com.jmpr.beans.mapstruct.BeanAOut;
import com.jmpr.beans.mapstruct.BeanBIn;
import com.jmpr.beans.mapstruct.BeanBOut;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * This mapper will generate dynamic runtime implementations by MapStruct. However, if you need some custom logic in
 * mappings, you can add methods manually using abstract class instead of an interface.
 *
 * @author Juan Manuel Pérez Rodríguez
 */

@Mapper
public abstract class ACMapper {
    public static ACMapper INSTANCE = Mappers.getMapper(ACMapper.class);

    // For mappings of fields with the same name it's not necessary an explicit mapping (i.e. field3)
    @Mappings({
            @Mapping(source = "field1", target = "field1Out"),
            @Mapping(source = "field2", target = "field2Out")
    })
    public abstract BeanAOut mapBeanAToBeanAOut(BeanAIn source);

    // This is an implemented mapping method
    public BeanBOut beanBInToBeanBOut(BeanBIn in) {
        BeanBOut out = new BeanBOut();
        out.setField1Out(in.getField1() + ".modified");
        out.setField2Out(in.getField2() + ".modified");
        out.setField3(in.getField1() + in.getField2());

        return out;
    }

}
