package com.jmpr.pocs.mapstruct.mapper;

import com.jmpr.beans.mapstruct.*;
import com.jmpr.pocs.mapstruct.enums.EnumIn;
import com.jmpr.pocs.mapstruct.enums.EnumOut;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.Date;
import java.util.List;

/**
 * This mapper will generate dynamic runtime implementations by MapStruct
 *
 * @author Juan Manuel Pérez Rodríguez
 */

@Mapper
public interface IMapper {
    IMapper INSTANCE = Mappers.getMapper(IMapper.class);

    // For mappings of fields with the same name it's not necessary an explicit mapping (i.e. field3)
    @Mappings({
            @Mapping(source = "field1", target = "field1Out"),
            @Mapping(source = "field2", target = "field2Out")
    })
    BeanAOut mapBeanAInToBeanAOut(BeanAIn source);

    // For mappings of fields with the same name it's not necessary an explicit mapping (i.e. field3)
    @Mappings({
            @Mapping(source = "field1", target = "field1Out"),
            @Mapping(source = "field2", target = "field2Out")
    })
    BeanBOut mapBeanBInToBeanBOut(BeanBIn source);

    @Mappings({
            @Mapping(source = "source1.field1", target = "fieldA1Out"),
            @Mapping(source = "source1.field2", target = "fieldA2Out"),
            @Mapping(source = "source1.field3", target = "fieldA3"),
            @Mapping(source = "source2.field1", target = "fieldB1Out"),
            @Mapping(source = "source2.field2", target = "fieldB2Out"),
            @Mapping(source = "source2.field3", target = "fieldB3"),
    })
    BeanABOut mapBeanAinAndBeanBinToBeanABOut(BeanAIn source1, BeanBIn source2);

    @Mappings({
            @Mapping(source = "source.field1.field3", target = "fieldCOut1"),
            @Mapping(source = "source.field2.field3", target = "fieldCOut2")
    })
    BeanCOut mapBeanCInToBeanCOut(BeanCIn source);

    @Mappings({
            @Mapping(source = "field3", target = "fieldCOut1")
    })
    void mapBeanAinToBeanCOut(BeanAIn source, @MappingTarget BeanCOut target);

    @Mappings({
            @Mapping(source = "field3", target = "fieldCOut2")
    })
    void mapBeanBinToBeanCOut(BeanBIn source, @MappingTarget BeanCOut target);

    @Mappings({
            @Mapping(source = "date", dateFormat = "dd/MM/yyyy", target = "field1"),
            @Mapping(source = "integer", target = "field2"),
            @Mapping(source = "floating", target = "field3"),
    })
    BeanDOut mapBeanDinToBeanDOut(BeanDIn source);

    @IterableMapping(dateFormat = "dd/MM/yyyy")
    List<String> stringListToDateList(List<Date> dates);

    BeanEOut mapBeanE(BeanEIn in);

    BeanFOut mapBeanF(BeanFIn in);

    @Mappings({
            @Mapping(source = "VALUE_IN_1", target = "VALUE_OUT_3"),
            @Mapping(source = "VALUE_IN_2", target = "VALUE_OUT_2"),
            @Mapping(source = "VALUE_IN_3", target = "VALUE_OUT_1")
    })
    EnumOut mapEnumInToEnumOut (EnumIn in);

    BeanGOut mapBeanG(BeanGIn in);

    @Mappings({
            @Mapping(target = "field1Out", constant = "constant1"),
            @Mapping(target = "field2Out", constant = "constant2"),
            @Mapping(target = "field3", constant = "constant3")
    })
    BeanAOut mapConstants(BeanBIn in);
}
